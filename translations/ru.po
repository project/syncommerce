# Russian translation of Syncart (8.x-1.0)
# Copyright (c) 2018 by the Russian translation team
#
msgid ""
msgstr ""
"Project-Id-Version: Syncart (8.x-1.0)\n"
"POT-Creation-Date: 2018-06-28 18:28+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Language-Team: Russian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Product management"
msgstr "Управление товарами"

msgid "Manage products"
msgstr "Управлять товарами"

msgid "Save product"
msgstr "Сохранить товар"

msgid "Save variation"
msgstr "Сохранить вариацию"
