/**
 * @file
 * Vue-cart index.js.
 */

// const store = new Vuex.Store({
// state: {
//   cart: {
//     items: {},
//     quantity: 0,
//     total: 0,
//   },
//   favorite: {
//     total: 0,
//   },
//   color: 0,
//   custom : '',
// },
// mutations: {
//   updateCart(state, cart) {
//     state.cart = cart;
//   },
//   updateColor(state, color) {
//     state.color = color;
//   },
//   updateCustom(state, custom) {
//     state.custom = custom;
//   },
//   updateFavorite(state, fav) {
//     state.favorite = fav;
//   },
// },
// });

window.addEventListener("DOMContentLoaded", function () {
  vueCartApp(".product-variation");
});

function vueCartApp(el) {
  "use strict";
  const delimiters = ["${", "}"];
  const comments = true;
  const elements = document.querySelectorAll(el);
  const each = Array.prototype.forEach;
  each.call(elements, (el, i) => {
    new Vue({ delimiters, comments, store, el });
  });
}
