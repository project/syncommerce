/**
 * @file
 * Vue app for cart.
 */

(function () {
  "use strict";

  Vue.component("product", {
    data() {
      return {
        ready: false,
        products: [],
        catalog: [],
        attributes: [],
        filters: {
          article: '',
          name: '',
          status: true,
          catalog: '',
          max: 100,
          page: 1,
        },
      };
    },
    computed: {
      // cart() {
      //   return this.$store.state.cart;
      // },
    },
    methods: {
      initCart() {
        let pathPrefix = drupalSettings.path.pathPrefix;
        let loadPath = "/syncommerce/products_list";
        let path = drupalSettings.path.currentPath;
        axios
          .post(loadPath, {
            filters: this.filters,
          })
          .then((response) => {
            // this.$store.commit("updateCart", response.data);
            if (response.data.quantity > 0) {
              this.ready = true;
            }
            this.products = response.data.products;
            this.catalog = response.data.catalog;
            this.attributes = response.data.attributes;
          });
      },
      filterApply() {
        // if (this.filters.name.length > 2) {
        this.initCart();
        // }
      },
      // changeQuantity(itemId, quantity, step) {
      //   quantity = !quantity || quantity < step ? step : quantity;
      //   let itemPos = false;
      //   for (var item in this.$store.state.cart.items) {
      //     if (this.$store.state.cart.items[item].id == itemId) {
      //       itemPos = item;
      //     }
      //   }
      //   if (itemPos) {
      //     let stock = this.$store.state.cart.items[itemPos].stock;
      //     if (!this.$store.state.cart.items[itemPos].check_store ||
      //       (this.$store.state.cart.items[itemPos].check_store && quantity <= stock)) {
      //       axios
      //         .post("/cart/set-product-quantity", {
      //           vid: itemId,
      //           quantity: quantity,
      //         })
      //         .then((response) => {
      //           this.$store.commit("updateCart", response.data);
      //         });
      //     }
      //   }
      // },
      saveProduct(id) {
        axios.post("/syncommerce/updateProduct", {
          id: id,
          article: this.products[id].article,
          title: this.products[id].title,
          status: this.products[id].status,
          catalog: this.products[id].catalog,
        }).then(
          (response) => {
            this.products[id].info = response.data;
            setTimeout(() => {
              this.products[id].info = '';
            }, 3000);
          },
        );
      },
      saveVariation(productId, variationId) {
        let attributes = new Object;
        for (let attribute in this.products[productId].variations[variationId].attributes) {
          attributes[attribute] = this.products[productId].variations[variationId].attributes[attribute].id;
        }
        axios.post("/syncommerce/updateVariation", {
          id: variationId,
          price_number: this.products[productId].variations[variationId].price_number,
          stock: this.products[productId].variations[variationId].stock,
          status: this.products[productId].variations[variationId].status,
          attributes: attributes,
        }).then(
          (response) => {
            this.products[productId].variations[variationId].info = response.data;
            setTimeout(() => {
              this.products[productId].variations[variationId].info = '';
            }, 3000);
          },
        );
      },
      numberFormat(number, step, decimals = 2, dec_point = ".", thousands_sep = " ") {
        let s_number =
          Math.abs(parseInt((number = (+number || 0).toFixed(decimals)))) + "";
        let len = s_number.length;
        let tchunk = len > 3 ? len % 3 : 0;
        let ch_first = tchunk ? s_number.substr(0, tchunk) + thousands_sep : "";
        let ch_rest = s_number
          .substr(tchunk)
          .replace(/(\d\d\d)(?=\d)/g, "$1" + thousands_sep);
        let ch_last = decimals
          ? dec_point + (Math.abs(number) - s_number).toFixed(decimals).slice(2)
          : "";
        if (step == 1) {
          ch_last = '';
        }
        return ch_first + ch_rest + ch_last;
      },
      isEmpty(value) {
        if (!!value && value instanceof Array) {
          return value.length < 1;
        }
        if (!!value && typeof value === "object") {
          for (var key in value) {
            if (hasOwnProperty.call(value, key)) {
              return false;
            }
          }
        }
        return !value;
      },
      readQuantity(e, item) {
        let quantity = e.target.innerText;
        quantity = parseInt(quantity.replaceAll(" ", ""));
        if (quantity > 999) {
          quantity = 999;
        } else if (quantity < 1) {
          quantity = 1;
        }
        this.changeQuantity(item.id, quantity, item.qstep);
      },
    },
    created() {
      this.initCart();
    },
  });

  String.prototype.replaceAll = function (search, replacement) {
    return this.split(search).join(replacement);
  };
})();
